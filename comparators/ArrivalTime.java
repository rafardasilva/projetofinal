package br.unb.oo.driversevaluation.comparators;

import java.util.Comparator;

import br.unb.oo.driversevaluation.domain.Driver;

public class ArrivalTime implements Comparator<Driver> {

	@Override
	public int compare(Driver o1, Driver o2) {
		if(o1.getMinorArrivalTime().after(o2.getMinorArrivalTime())) return 1;
		if(o1.getMinorArrivalTime().before(o2.getMinorArrivalTime())) return -1;
		return 0;
	}

}
