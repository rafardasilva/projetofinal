package br.unb.oo.driversevaluation.comparators;

import java.util.Comparator;

import br.unb.oo.driversevaluation.domain.Driver;

public class MinutesOfMotorMisuse implements Comparator<Driver> {

	@Override
	public int compare(Driver o1, Driver o2) {
		if(o1.getAvgMinutesOfMotorMisuse() < o2.getAvgMinutesOfMotorMisuse()) return 1;
		if(o1.getAvgMinutesOfMotorMisuse() > o2.getAvgMinutesOfMotorMisuse()) return -1;
		return 0;
	}

}
