package br.unb.oo.driversevaluation.main;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.unb.oo.driversevaluation.domain.Driver;
import br.unb.oo.driversevaluation.domain.TravelRecord;
import br.unb.oo.driversevaluation.util.Printer;

@SuppressWarnings("unchecked")
public class Main {

	// Caminho para a pasta raiz com os dados dos motoristas
	private static String folderPath = "C:/Rafael/workspace/drivers-evaluation/txt";

	public static <UnixPath> void main(String[] args) throws IOException, ParseException {
		List<UnixPath> filesAsAbstractList = (List<UnixPath>) Arrays.asList(Files.walk(Paths.get(folderPath)).toArray());

		
		ArrayList<UnixPath> files = new ArrayList<UnixPath>();
		files.addAll(filesAsAbstractList);
		
		
		files.remove(0);
		
		
		List<Driver> drivers = new ArrayList<Driver>();
		
		for (UnixPath file : files) {
			// Converte todas as linhas do arquivo em uma lista de Strings onde cada
			// elemento corresponde a uma linha
			List<String> dataAsLine = Files.readAllLines((Path) file);
			drivers.add(readDriverRecords(dataAsLine));
		}

		
		
		// Imprime a lista de drivers
		Printer.printMinutesOfMotorMisuseRanking(drivers);
		Printer.printVelocityRanking(drivers);
		Printer.printClutchAngleRanking(drivers);
		Printer.printBreakAngleRanking(drivers);
		Printer.printDepartureTimeRanking(drivers);
		Printer.printArrivalTimeRanking(drivers);
		Printer.printRotationRanking(drivers);
		Printer.printTrasmissionChangeTimeRanking(drivers);
		Printer.printDriversInfo(drivers);
	}

	private static Driver readDriverRecords(List<String> dataAsLines) throws ParseException {
		// Instancia uma lista vazia de drivers que sera preenchida com os
		// valores do .txt
		List<TravelRecord> records = new ArrayList<TravelRecord>();
		
		// Instancia uma variavel de controle que representara o id dos
		// motoristas
		Integer recordId = 0;

		String name = dataAsLines.get(0);
		dataAsLines.remove(0);
		
		// Loop que itera por cada linha do arquivo, divide os atributos por
		// virgulas e instancia um novo objeto DriverInfo
		for (String line : dataAsLines) {

			if (line.isEmpty() || line == null) {
				continue;
			}

			// Divide a linha usando virgulas como separador
			String[] dataAsArray = line.split(",");

			
			String minutesOfMotorMisuse = dataAsArray[0];
			String velocity = dataAsArray[1];
			String clutchAngle = dataAsArray[2];
			String breakAngle = dataAsArray[3];
			String rotation = dataAsArray[4];
			String departureTime = dataAsArray[5];
			String arrivalTime = dataAsArray[6];
			String timeOfTransmissionChange = dataAsArray[7];

			// Instancia um novo DriverInfo e adiciona na lista de drivers
			records.add(new TravelRecord(recordId++, minutesOfMotorMisuse,
					velocity, clutchAngle, breakAngle, rotation, departureTime,
					arrivalTime, timeOfTransmissionChange));
		}
		
		return new Driver(name, records);
	}
}
