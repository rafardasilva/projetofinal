package br.unb.oo.driversevaluation.util;

import java.util.Collections;
import java.util.List;

import br.unb.oo.driversevaluation.comparators.ArrivalTime;
import br.unb.oo.driversevaluation.comparators.BreakAngle;
import br.unb.oo.driversevaluation.comparators.ClutchAngle;
import br.unb.oo.driversevaluation.comparators.DepartureTime;
import br.unb.oo.driversevaluation.comparators.MinutesOfMotorMisuse;
import br.unb.oo.driversevaluation.comparators.Rotation;
import br.unb.oo.driversevaluation.comparators.TrasmissionChangeTime;
import br.unb.oo.driversevaluation.comparators.Velocity;
import br.unb.oo.driversevaluation.domain.Driver;

public final class Printer {

	public static void print(List<Driver> drivers) {
		updateDrivers(drivers);
		
		// Inicia a posicao no rank em 1
		Integer index = Integer.valueOf(1);

		for (Driver driver : drivers) {
			// Imprime a posicao no rank e o nome do motorista
			System.out.println(index + ". " + driver.getName());

			// Incrementa a posicao no rank
			index++;
		}

		// Imprime linha em branco para formatação
		System.out.println("");
	}

	public static void printMinutesOfMotorMisuseRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		
		System.out.println("Ranking 'Tempo médio de desuso do motor' (Crescente)");

		// Ordena a lista de drivers seguindo o criterio da classe
		// MinutesOfMotorMisuse que implementa um comparador que será utilizado
		// pelo método Collections.sort para organizar a lista
		Collections.sort(drivers, new MinutesOfMotorMisuse());

		// Chama o método que print o ranking passando a lista após a ordenação
		print(drivers);
	}

	private static void updateDrivers(List<Driver> drivers) {
		for (Driver driver : drivers) {
			driver.updateAveragesAndPoints();
		}
	}

	public static void printVelocityRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Ranking 'Velocidade Média' (Crescente)");
		Collections.sort(drivers, new Velocity());
		print(drivers);
	}

	public static void printClutchAngleRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Ranking 'Ângulo Embreagem' (Crescente)");
		Collections.sort(drivers, new ClutchAngle());
		print(drivers);
	}

	public static void printBreakAngleRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Ranking 'Ângulo Freio' (Crescente)");
		Collections.sort(drivers, new BreakAngle());
		print(drivers);
	}

	public static void printRotationRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Ranking 'Rotação' (Crescente)");
		Collections.sort(drivers, new Rotation());
		print(drivers);
	}

	public static void printDepartureTimeRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Ranking 'Hora de partida' (Crescente)");
		Collections.sort(drivers, new DepartureTime());
		print(drivers);
	}

	public static void printArrivalTimeRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Ranking 'Hora de chegada' (Crescente)");
		Collections.sort(drivers, new ArrivalTime());
		print(drivers);
	}

	public static void printTrasmissionChangeTimeRanking(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Ranking 'Tempo de troca de marcha' (Decrescente)");
		Collections.sort(drivers, new TrasmissionChangeTime());
		print(drivers);
	}

	public static void printDriversInfo(List<Driver> drivers) {
		updateDrivers(drivers);
		System.out.println("Lista de Motoristas ---------------");
		
		for (Driver driver : drivers) {
			System.out.println(driver);
		}
	}

}
