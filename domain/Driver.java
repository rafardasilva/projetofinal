package br.unb.oo.driversevaluation.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Driver {

	private String name;
	private List<TravelRecord> records;

	private Double avgMinutesOfMotorMisuse;
	private Double avgVelocity;
	private Integer clutchAngleUnder15;
	private Integer breakAngleUnder15;
	private Double avgRotation;
	private Date minorDepartureTime;
	private Date minorArrivalTime;
	private Double avgTimeOfTransmissionChange;

	public Driver(String name, List<TravelRecord> records) {
		this.name = name;
		this.records = records;
		this.avgMinutesOfMotorMisuse = Double.valueOf(0);
		this.avgVelocity = Double.valueOf(0);
		this.clutchAngleUnder15 = Integer.valueOf(0);
		this.breakAngleUnder15 = Integer.valueOf(0);
		this.avgRotation = Double.valueOf(0);
		this.minorDepartureTime = new Date();
		this.minorArrivalTime = new Date();
		this.avgTimeOfTransmissionChange = Double.valueOf(0);
	}

	public String getName() {
		return name;
	}
	
	public List<TravelRecord> getRecords() {
		return records;
	}

	public Double getAvgMinutesOfMotorMisuse() {
		return avgMinutesOfMotorMisuse;
	}

	public Double getAvgVelocity() {
		return avgVelocity;
	}

	public Integer getClutchAngleUnder15() {
		return clutchAngleUnder15;
	}

	public Integer getBreakAngleUnder15() {
		return breakAngleUnder15;
	}

	public Double getAvgRotation() {
		return avgRotation;
	}

	public Date getMinorDepartureTime() {
		return minorDepartureTime;
	}

	public Date getMinorArrivalTime() {
		return minorArrivalTime;
	}

	public Double getAvgTimeOfTransmissionChange() {
		return avgTimeOfTransmissionChange;
	}

	public void setAvgMinutesOfMotorMisuse(Double avgMinutesOfMotorMisuse) {
		this.avgMinutesOfMotorMisuse = avgMinutesOfMotorMisuse;
	}

	public void setAvgVelocity(Double avgVelocity) {
		this.avgVelocity = avgVelocity;
	}

	public void setClutchAngleUnder15(Integer clutchAngleUnder15) {
		this.clutchAngleUnder15 = clutchAngleUnder15;
	}

	public void setBreakAngleUnder15(Integer breakAngleUnder15) {
		this.breakAngleUnder15 = breakAngleUnder15;
	}

	public void setAvgRotation(Double avgRotation) {
		this.avgRotation = avgRotation;
	}

	public void setMinorDepartureTime(Date minorDepartureTime) {
		this.minorDepartureTime = minorDepartureTime;
	}

	public void setMinorArrivalTime(Date minorArrivalTime) {
		this.minorArrivalTime = minorArrivalTime;
	}

	public void setAvgTimeOfTransmissionChange(
			Double avgTimeOfTransmissionChange) {
		this.avgTimeOfTransmissionChange = avgTimeOfTransmissionChange;
	}

	public void updateAveragesAndPoints() {
		this.avgMinutesOfMotorMisuse = calculateAvgMinutesOfMotorMisuse();
		this.avgVelocity = calculateAvgVelocity();
		this.clutchAngleUnder15 = calculateClutchAngleUnder15();
		this.breakAngleUnder15 = calculateBreakAngleUnder15();
		this.avgRotation = calculateAvgRotation();
		this.minorDepartureTime = calculateMinorDepartureTime();
		this.minorArrivalTime = calculateMinorArrivalTime();
		this.avgTimeOfTransmissionChange = Double.valueOf(0);
	}

	private Date calculateMinorArrivalTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		
		Date minorArrivalTime = null;
		try {
			minorArrivalTime = formatter.parse("23:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		for (TravelRecord record : records) {
			Date arrivalTime = record.getArrivalTime();
			if(minorArrivalTime.after(arrivalTime)) minorArrivalTime = arrivalTime;
		}
		
		return minorArrivalTime;
	}

	private Date calculateMinorDepartureTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		
		Date minorDepartureTime = null;
		try {
			minorDepartureTime = formatter.parse("23:59");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (TravelRecord record : records) {
			Date departureTime = record.getDepartureTime();
			if(minorDepartureTime.after(departureTime)) minorDepartureTime = departureTime;
		}
		
		return minorDepartureTime;
	}

	private Double calculateAvgRotation() {
		Double total = Double.valueOf(0);

		for (TravelRecord record : records) {
			total += record.getRotation();
		}

		return total / records.size();
	}

	private Integer calculateBreakAngleUnder15() {
		Integer total = Integer.valueOf(0);

		for (TravelRecord record : records) {
			if (record.getBreakAngle() < 15)
				total++;
		}

		return total;
	}

	private Integer calculateClutchAngleUnder15() {
		Integer total = Integer.valueOf(0);

		for (TravelRecord record : records) {
			if (record.getClutchAngle() < 15)
				total++;
		}

		return total;
	}

	private Double calculateAvgVelocity() {
		Double total = Double.valueOf(0);

		for (TravelRecord record : records) {
			total += record.getVelocity();
		}

		return total / records.size();
	}

	private Double calculateAvgMinutesOfMotorMisuse() {
		Double total = Double.valueOf(0);

		for (TravelRecord record : records) {
			total += record.getMinutesOfMotorMisuse();
		}

		return total / records.size();
	}

	@Override
	public String toString() {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");		
		return "\nMotorista: " + name + 
				"\nVelocidade média: " + avgVelocity +
				"\nMenor hora de chegada: " + formatter.format(minorDepartureTime) +
				"\nMenor hora de partida: " + formatter.format(minorArrivalTime) + 
				"\nRotação média: " + avgRotation +
				"\nÂngulo freio abaixo de 15: " + breakAngleUnder15 +
				"\nÂngulo embreagem abaixo de 15: " + clutchAngleUnder15 + 
				"\nMédia do tempo de troca de marcha: " + avgTimeOfTransmissionChange + "\n";
	}

	
	
}
