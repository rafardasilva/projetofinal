package br.unb.oo.driversevaluation.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TravelRecord {
	private static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

	private Integer id;
	private Double minutesOfMotorMisuse;
	private Double velocity;
	private Double clutchAngle;
	private Double breakAngle;
	private Double rotation;
	private Date departureTime;
	private Date arrivalTime;
	private Double timeOfTransmissionChange;

	public TravelRecord(Integer id, String minutesOfMotorMisuse,
			String velocity, String clutchAngle, String breakAngle,
			String rotation, String departureTime, String arrivalTime,
			String timeOfTransmissionChange) throws ParseException {
		
		this.id = id;
		this.minutesOfMotorMisuse = Double.parseDouble(minutesOfMotorMisuse);
		this.velocity = Double.parseDouble(velocity);
		this.clutchAngle = Double.parseDouble(clutchAngle);
		this.breakAngle = Double.parseDouble(breakAngle);
		this.rotation = Double.parseDouble(rotation);
		this.departureTime = formatter.parse(departureTime);
		this.arrivalTime = formatter.parse(arrivalTime);
		this.timeOfTransmissionChange = Double.parseDouble(timeOfTransmissionChange);
	}

	public Integer getId() {
		return id;
	}

	public Double getVelocity() {
		return velocity;
	}

	public Double getClutchAngle() {
		return clutchAngle;
	}

	public Double getBreakAngle() {
		return breakAngle;
	}

	public Double getRotation() {
		return rotation;
	}
	
	public Double getMinutesOfMotorMisuse() {
		return minutesOfMotorMisuse;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public Double getTimeOfTransmissionChange() {
		return timeOfTransmissionChange;
	}

	@Override
	public String toString() {
		return "\nRegistro #" + this.id
				+ "\nTempo de veículo parado: " + this.minutesOfMotorMisuse
				+ "\nVelocidade: " + this.velocity
				+ "\nTempo com o pé na embreagem: " + this.clutchAngle
				+ "\nTempo com o pé no freio: " + this.breakAngle
				+ "\nRotação: " + this.rotation
				+ "\nHora de Partida: " + formatAsTime(this.departureTime)
				+ "\nHora de Chegada: " + formatAsTime(this.arrivalTime)
				+ "\nTempo de troca de marcha: " + this.timeOfTransmissionChange + "\n";
	}
	
	private String formatAsTime(Date date){
		return formatter.format(date);
	}
}
